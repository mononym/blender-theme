Combination of tin2tin's [Desaturated](https://gist.github.com/tin2tin/378707cc4062cf862289e4cd49d24daa) and venomgfx's [Flatty Dark Blueberry](https://github.com/venomgfx/blender-themes/blob/master/flatty_dark_blueberry.xml) themes.

![screenshot](img/screenshot.png)